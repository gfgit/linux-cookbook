## KIO Admin

Dolphin (KDE File manager) refuses to run as root.
To navigate and edit system folders you need KIO Admin
module which adds a context menu "Open with KIO Admin" and will ask you admin password.
```
sudo apt install kio-admin
```

## KDE Plasma theme

+ See [BUG 444043](https://bugs.kde.org/show_bug.cgi?id=444043)

## KDE Plasma taskbar

- Disable mouse wheel looping through activities
- Disable icon grouping

## KDE Web search

* Open KDE System Settings
* Go to Search
* Go to Plasma Search
* Go to keyword web search
* Click configure
* Disable all Big Data search engines
* Set DDG url with parameters as done in <a href="./web_browsers.md#ddg-url-parameters">Firefox section</a>
* Set DDG url with parameters as done in [Firefox section](./web_browsers.md#Search Engine Helper)

## GTK+ Applications with KDE File Dialog

Add `export GTK_USE_PORTAL=1` to `.profile` and logout/login [Arch Wiki](https://wiki.archlinux.org/title/Uniform_look_for_Qt_and_GTK_applications#Consistent_file_dialog_under_KDE_Plasma)

## Eyes Protection

+ Enable RedShift
+ [SafeEyes](https://slgobinath.github.io/SafeEyes/)

